$(function () {
  var mainHeader = document.querySelector("#main-header");
  var headroom = new Headroom(mainHeader);
  headroom.init();

  // Menú responsive
  let ancho = $(window).width();
  let links = $('#links');
  let btnMenu = $('#btn-menu');
  let btnMenuIcon = $('#btn-menu .btn-menu--icon');

  if (ancho < 700) {
    links.hide();
    btnMenuIcon.addClass('fa-bars');
  }

  btnMenu.on('click', function (e) {
    links.slideToggle();
    btnMenuIcon.toggleClass('fa-bars');
    btnMenuIcon.toggleClass('fa-times');
  })

  $(window).on('resize', function () {
    if ($(this).width() > 700) {
      links.show();
      btnMenuIcon.addClass('fa-times');
      btnMenuIcon.removeClass('fa-bars');
    } else {
      links.hide();
      btnMenuIcon.addClass('fa-bars');
      btnMenuIcon.removeClass('fa-times');
    }
  })
});
