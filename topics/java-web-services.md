---
layout: default
title: Web Services
permalink: topics/java-web-services.html
---

# Web Services

__Web Service__ - A web service is a kind of software that is accessible on the
 Internet. It makes use of the XML messaging system and offers an easy to
 understand, interface for the end users.

__Advantages of Web Services__

 * Interoperability: Web services are accessible over network and runs on
   HTTP/SOAP protocol and uses XML/JSON to transport data, hence it can be
   developed in any programming language. Web service can be written in Java
   programming and client can be PHP and vice versa.
 * Reusability: One web service can be used by many client applications at the
   same time.
 * Loose Coupling: Web services client code is totally independent with server
   code, so we have achieved loose coupling in our application.
 * Easy to deploy and integrate, just like web applications.
 * Multiple service versions can be running at same time.

__Types of Web Services__ - There are two types of web services:

 * __SOAP Web Services__: Runs on SOAP protocol and uses XML technology for sending data.

 * __Restful Web Services__: It’s an architectural style and runs on HTTP/HTTPS
   protocol almost all the time. REST is a stateless client-server architecture
   where web services are resources and can be identified by their URIs. Client
   applications can use HTTP GET/POST methods to invoke Restful web services.

__SOAP__ - SOAP stands for Simple Object Access Protocol. SOAP is an XML based
 industry standard protocol for designing and developing web services. Since
 it’s XML based, it’s platform and language independent.

__Advantages SOAP Web Services__ - SOAP web services have all the advantages
 that web services has, some of the additional advantages are:

 * WSDL document provides contract and technical details of the web services
   for client applications without exposing the underlying implementation
   technologies.
 * SOAP uses XML data for payload as well as contract, so it can be easily
   read by any technology.
 * SOAP protocol is universally accepted, so it’s an industry standard approach
   with many easily available open source implementations.

__Disadvantages of SOAP Web Services__ - Some of the disadvantages of SOAP
 protocol are:

 * Only XML can be used, JSON and other lightweight formats are not supported.
 * SOAP is based on the contract, so there is a tight coupling between client
   and server applications.
 * SOAP is slow because payload is large for a simple string message, since it
   uses XML format.
 * Anytime there is change in the server side contract, client stub classes
   need to be generated again.
 * Can’t be tested easily in browser

__REST__- REST stand for REpresentational State Transfer, term by Roy Fielding.
 REST is an architectural style that describes best practices to expose web
 services over HTTP with Emphasizes scalability.

__What is RESTful web services__ - Web services written by apply REST
 Architectural concept are called RESTful web services which focus on System
 resources and how state of Resource should be transferred over http protocol to
 a different clients written in different languages. In RESTful web services
 HTTP methods like GET, PUT, POST and DELETE can be used to perform CRUD
 operations.  

__Differences between RESTful web services and SOAP web services:__

 *  REST is more simple and easy to use than SOAP.
 *  REST uses HTTP protocol for producing or consuming web services while
    SOAP uses XML.
 *  REST is lightweight as compared to SOAP and preferred choice in mobile
    devices and PDA’s.
 *  REST supports different format like text, JSON and XML while SOAP only
    support XML.
 *  REST web services call can be cached to improve performance.

__REST Web Services__ - REST is the acronym for Representational State Transfer.
 REST is an architectural style for developing applications that can be accessed
 over the network. REST architectural style was brought in light by Roy Fielding
 in his doctoral thesis in 2000.

__Advantages of REST web services__ - Some of the advantages of REST web
 services are:

 * Learning curve is easy since it works on HTTP protocol
 * Supports multiple technologies for data transfer such as text, xml, json,
   image etc.
 * No contract defined between server and client, so loosely coupled
   implementation.
 * REST is a lightweight protocol
 * REST methods can be tested easily over browser.

__Disadvantages of REST web services__ - Some of the disadvantages of REST are:
 * Since there is no contract defined between service and client, it has to be
   communicated through other means such as documentation or emails.
 * Since it works on HTTP, there can’t be asynchronous calls.
 * Sessions can’t be maintained.

__Different HTTP Methods supported in Restful Web Services__ - Restful web
 services supported HTTP methods are – GET, POST, PUT, DELETE and HEAD.

__Resource__ - The key abstraction of information in REST is a resource. Any
 information that can be named can be a resource like Student, Employee etc.
 REST is resource based API. Expose resources through URIs like
 `http://localhost:8080/api/employees/1111`.

__REST operations__ - Safe REST operations are HTTP methods that do not modify
 resources. For example, using GET or HEAD on a resource URL, should NEVER
 change the resource.

__Idempotent operations__ - Idempotent operations are those HTTP methods that
 can be called repetitively many times without different outcomes. It would not
 matter if the method is called only once, or many times over.

__Idempotency important__ - Idempotency is important in building a
 fault-tolerant API. Suppose a client wants to update a resource through POST.
 Since POST is not a idempotent method, calling it multiple times can result in
 wrong updates.

__SOAP__ - Is XML based protocol. It is platform independent and language
 independent. By using SOAP, you will be able to interact with other programming
 language applications.

__WSDL__ - WSDL stands for Web Service Description Language. It is an XML file
 that describes the technical details of how to implement a web service, more
 specifically the URI,port, method names, arguments, and data types. Since
  WSDL is XML, it is both human-readable and machine-consumable.

__Different components of WSDL__ - Some of the different tags in WSDL xml are:
 *  xsd:import namespace and schemaLocation: provides WSDL URL and unique
   namespace for web service.
 *  message: for method arguments
 *  part: for method argument name and type
 *  portType: service name, there can be multiple services in a wsdl document.
 *  operation: contains method name
 *  soap:address for endpoint URL.

__Different ways to test web services:__
 * SOAP web services can be tested programmatically by generating client stubs
   from WSDL or through software such as Soap UI.
 * REST web services can be tested easily with program, curl commands and
   through browser extensions. Resources supporting GET method can be tested
   with browser itself, without any program.

__Can we maintain user session in web services__ - Web services are stateless
 so we can’t maintain user sessions in web services.

__Difference between SOA and Web Services__ - Service Oriented Architecture
 (SOA) is an architectural pattern where applications are designed in terms of
 services that can be accessed through communication protocol over network. SOA
 is a design pattern and doesn’t go into implementation.

__Use of Accept and Content-Type Headers in HTTP Request__ - These are important
 headers in Restful web services. Accept headers tells web service what kind of
 response client is accepting, so if a web service is capable of sending
 response in XML and JSON format and client sends Accept header as
  “`application/xml`” then XML response will be sent. For Accept header
  “`application/json`”, server will send the JSON response. Content-Type header
  is used to tell server what is the format of data being sent in the request.
  If Content-Type header is “application/xml” then server will try to parse it
  as XML data. This header is useful in HTTP Post and Put requests.

__UDDI__ - UDDI is acronym for Universal Description, Discovery and Integration.
 UDDI is a directory of web services where client applications can lookup for
 web services. Web Services can register to the UDDI server and make them
 available to client applications.  

 * UDDI stands for Universal Description, Discovery, and Integration.
 * UDDI is a specification for a distributed registry of web services.
 * UDDI is a platform-independent, open framework.
 * UDDI can communicate via SOAP, CORBA, Java RMI Protocol.
 * UDDI uses Web Service Definition Language(WSDL) to describe interfaces to web
   services.
 * UDDI is seen with SOAP and WSDL as one of the three foundation standards of
   web services.
 * UDDI is an open industry initiative, enabling businesses to discover each
   other and define how they interact over the Internet.

__Difference between Top Down and Bottom Up approach in SOAP Web Services__ - In
 Top Down approach first WSDL document is created to establish the contract
 between web service and client and then code is written, it’s also termed as
 contract first approach.

__How would you choose between SOAP and REST web services__ - Web Services work
 on clientserver model and when it comes to choose between SOAP and REST, it all
 depends on project requirements. Let’s look at some of the conditions affecting
 our choice:
 * Do you know your web service clients beforehand? If Yes, then you can define
   a contract before implementation and SOAP seems better choice. But if you
   don’t then REST seems better choice because you can provide sample
   request/response and test cases easily for client applications to use later
   on.
 * How much time you have? For quick implementation REST is the best choice. You
   can create web service easily, test it through browser/curl and get ready for
   your clients.
 * What kind of data format are supported? If only XML then you can go with SOAP
   but if you think about supporting JSON also in future then go with REST.

__JAX-WS API__ - JAX-WS stands for Java API for XML Web Services. JAX-WS is XML
 based Java API to build web services server and client application. It’s part
 of standard Java API, so we don’t need to include anything else which working
 with it. Refer to JAX-WS Tutorial for a complete example.

__Name some frameworks in Java to implement SOAP web services__ - We can create
 SOAP web services using JAX-WS API, however some of the other frameworks that
 can be used are Apache Axis and Apache CXF. Note that they are not
 implementations of JAX-WS API, they are totally different framework that work
 on Servlet model to expose your business logic classes as SOAP web services.
 Read more at Java SOAP Web Service Eclipse example.

__Name important annotations used in JAX-WS API__ - Some of the important
 annotations used in JAX-WS API are:

 * `@WebService`
 * `@SOAPBinding`
 * `@WebMethod`

__What is use of `javax.xml.ws.Endpoint` class__ - Endpoint class provides
 useful methods to create endpoint and publish existing implementation as web
 service. This comes handy in testing web services before making further changes
 to deploy it on actual server. -RS Difference between RPC Style and Document Style SOAP web Services .-RPC style generate WSDL document based on the method name and it’s parameters. No type definitions are present in WSDL document. Document style contains type and can be validated against predefined schema. Let’s look at these with a simple program. Below is a simple test program where I am using Endpoint to publish my simple SOAP web service.

__How to get WSDL file of a SOAP web service?__ - WSDL document can be accessed by appending ? wsdl to the SOAP endoint URL. In above example, we can access it at http://localhost:8888/testWS? wsdl location.

__What is sun-jaxws.xml file?__ - This file is used to provide endpoints details when JAX-WS web services are deployed in servlet container such as Tomcat. This file is present in WEB-INF directory and contains endpoint name, implementation class and URL pattern.

__What is JAX-RS API?__ - Java API for RESTful Web Services (JAX-RS) is the Java API for creating REST web services. JAX-RS uses annotations to simplify the development and deployment of web services. JAX-RS is part of JDK, so you don’t need to include anything to use it’s annotations.

Name some implementations of JAX-RS API There are two major implementations of JAX-RS API.

 * Jersey: Jersey is the reference implementation provided by Sun. For using Jersey as our JAXRS implementation, all we need to configure its servlet in web.xml and add required dependencies. Note that JAX-RS API is part of JDK not Jersey, so we have to add its dependency jars in our application.
 * RESTEasy: RESTEasy is the JBoss project that provides JAX-RS implementation.

__What is wsimport utility__ - We can use wsimport utility to generate the client stubs. This utility comes with standard installation of JDK. Below image shows an example execution of this utility for one of JAX-WS project.

__Name important annotations used in JAX-RS API?__ Some of the important JAX-RS annotations are:

 * `@Path`: used to specify the relative path of class and methods. We can get the URI of a webservice by scanning the Path annotation value.
 * `@GET`, `@PUT`, `@POST`, `@DELETE` and `@HEAD`: used to specify the HTTP request type for a method.
 * `@Produces`, `@Consumes`: used to specify the request and response types.
 * `@PathParam`: used to bind the method parameter to path value by parsing it. What is the use of `@XmlRootElement` annotation .-XmlRootElement annotation is used by JAXB to transform java object to XML and vice versa. So we have to annotate model classes with this annotation.
